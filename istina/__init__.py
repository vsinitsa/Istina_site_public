
def get_model():
    from istina.models import CustomComment
    return CustomComment

def get_form():
    """
    Returns the comment ModelForm class.
    """
    from istina.forms import CustomCommentForm
    return CustomCommentForm