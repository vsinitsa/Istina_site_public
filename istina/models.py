# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User, AbstractUser
from django.utils import timezone
from django_comments.models import Comment

# Create your models here.

class User(AbstractUser):
    login_attempts = models.IntegerField(default=0)
    # active = models.BooleanField(default=False)

    def get_color(self):
        r = self.username
        r_color = '#%02X%02X%02X' % (ord(r[0])*1.6, ord(r[len(r)/2])*1.65, ord(r[-1])*1.7)
        return r_color

class Feedback(models.Model):
    feedback_id = models.AutoField(primary_key=True)
    user_id = models.IntegerField(blank=True, null=True)
    article_id = models.IntegerField()
    name = models.CharField(max_length=30)
    text = models.TextField()
    email = models.CharField(max_length=45)
    status = models.CharField(max_length=10)
    feedback_date = models.DateTimeField()

    def __unicode__(self):
        return "Feedback <%s -- %s -- %s>"%(self.feedback_id, self.email, self.text[:30]+'...')

    class Meta:
        db_table = 'feedback'


#XXX: Another old user model not used in our site
class FeedbackUsers(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    username = models.CharField(db_column='Username', max_length=50)  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=50)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=100)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=100)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=100, blank=True, null=True)  # Field name made lowercase.
    creationdate = models.DateTimeField(db_column='CreationDate')  # Field name made lowercase.
    active = models.IntegerField(db_column='Active')  # Field name made lowercase.
    activationcode = models.CharField(db_column='ActivationCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    passwordreset = models.IntegerField(db_column='PasswordReset', blank=True, null=True)  # Field name made lowercase.

    def __unicode__(self):
        return "FeedbackUser <%s -- %s -- %s>"%(self.id, self.username, self.email)

    class Meta:
        db_table = 'feedback_users'


class MagazineArticles(models.Model):
    article_id = models.IntegerField(primary_key=True)
    issue_id = models.IntegerField()
    topic_id = models.IntegerField()
    header = models.TextField()
    picture = models.CharField(max_length=20)
    picture1 = models.CharField(max_length=20)
    image = models.CharField(max_length=15)
    author_id = models.IntegerField()
    intro = models.TextField()
    text = models.TextField()
    article_status = models.CharField(max_length=10)
    article_date = models.DateTimeField()

    def __unicode__(self):
        return "MagazineArticle <%s -- %s -- %s>"%(self.article_id, self.issue_id, self.header[:10]+'...')

    def get_topic_text(self):
        text = None
        topic = MagazineTopics.objects.filter(topic_id=self.topic_id).first()
        if topic:
            text = topic.topic_text
        return text

    class Meta:
        db_table = 'magazine_articles'


class MagazineAuthors(models.Model):
    author_id = models.IntegerField(primary_key=True)
    picture = models.CharField(max_length=20)
    title = models.CharField(max_length=25)
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    place = models.CharField(max_length=30)
    intro = models.TextField()
    comments = models.TextField()
    registration_date = models.DateTimeField()

    def __unicode__(self):
        return "MagazineAuthor <%s -- %s -- %s>"%(self.author_id, self.last_name, self.first_name)

    class Meta:
        db_table = 'magazine_authors'


class MagazineIssues(models.Model):
    issue_id = models.IntegerField(primary_key=True)
    month = models.TextField()
    no_year = models.IntegerField()
    no_all = models.IntegerField()
    year = models.IntegerField()
    picture = models.CharField(max_length=20)
    intro = models.TextField()
    welcome = models.TextField()

    def __unicode__(self):
        return "MagazineIssue <%s -- %s>"%(self.issue_id, self.intro[:30]+'...')

    class Meta:
        db_table = 'magazine_issues'

class MagazineTopics(models.Model):
    topic_id = models.IntegerField(primary_key=True)
    topic_st = models.CharField(max_length=1)
    topic_text = models.TextField()

    def __unicode__(self):
        return "MagazineTopic <%s -- %s>"%(self.topic_id, self.topic_text[:10]+'...')

    class Meta:
        db_table = 'magazine_topics'

class News(models.Model):
    title = models.CharField(max_length=100)
    preface = models.TextField()
    article = models.TextField()
    preview_img_height = models.IntegerField()
    news_img_width = models.IntegerField()
    news_img_height = models.IntegerField()
    article_date = models.DateTimeField()
    active = models.IntegerField()

    def __unicode__(self):
        return "News <%s -- %s>"%(self.id, self.title[:30]+'...')

    class Meta:
        db_table = 'news'


#XXX: Probably old model from old istina site
class Users(models.Model):
    name = models.CharField(max_length=30)
    uname = models.CharField(max_length=30)
    pwd = models.CharField(max_length=30)
    level = models.IntegerField()

    def __unicode__(self):
        return "User <%s - %s - %s>"%(self.id, self.uname, self.name)

    class Meta:
        db_table = 'users'


class CustomComment(Comment):
    fav_count = models.BigIntegerField(default=0)

    # def save(self, *args, **kwargs):
    #     if self.submit_date is None:
    #         self.submit_date = timezone.now()
    #     super(Comment, self).save(*args, **kwargs)

    def __unicode__(self):
        return "CustomComment <%s -- %s -- %s %s>"%(self.id, self.user_name, self.comment[:10], self.submit_date)