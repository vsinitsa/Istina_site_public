from django_comments.forms import CommentDetailsForm
from . import get_model
from models import CustomComment

class CustomCommentForm(CommentDetailsForm):
    def get_comment_model(self):
        return get_model()