from django.contrib import admin
from django_comments.admin import CommentsAdmin

# Register your models here.

from .models import Feedback
from .models import FeedbackUsers
from .models import MagazineArticles, MagazineAuthors, MagazineIssues, MagazineTopics
from .models import News
from .models import Users
from .models import User
from .models import CustomComment

admin.site.register(Feedback)
admin.site.register(FeedbackUsers)
admin.site.register(MagazineIssues)
admin.site.register(MagazineAuthors)
admin.site.register(MagazineArticles)
admin.site.register(MagazineTopics)
admin.site.register(News)
admin.site.register(Users)
admin.site.register(User)

admin.site.register(CustomComment, CommentsAdmin)