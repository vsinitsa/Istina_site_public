# -*- coding: utf-8 -*-
import traceback

from django.contrib.contenttypes.models import ContentType
from django.core.validators import validate_email
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, HttpResponseForbidden
from django.conf import settings
from models import Users, FeedbackUsers, Feedback, MagazineArticles, MagazineAuthors, MagazineIssues, News, \
    MagazineTopics, User, CustomComment
import re
import time
import datetime
import os
from django.forms.models import model_to_dict
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, login
import json
from django.utils import timezone


from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from acct_token_generator import account_activation_token

# Create your views here.


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    next = request.POST.get('next', None)
    try:
        user = User.objects.get(Q(email__iexact=username)|Q(username__iexact=username))
        print "*******user 333: ", user
        if user.check_password(raw_password=password):
            user = user
        else:
            user = None
    except:
        user = None

    if user and user.is_active:
        login(request, user)
        if request.user.is_authenticated():
            if next:
                return HttpResponse(json.dumps({"next": next}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({"next": '/'}), content_type='application/json')
    elif user and not user.is_active:
        return HttpResponse(
            json.dumps({"errors": ["Пожалуйста, подтвердите свой email, чтобы завершить регистрацию. <a style='cursor:pointer' onclick='send_confirm_email()'>Отправить еще одно подтверждение.</a>"]}), content_type='application/json')
    else:
        return HttpResponse(
            json.dumps({"errors": ["<strong>Не удается войти.</strong> Пожалуйста, проверьте правильность написания логина и пароля."]}), content_type='application/json')


def home(request):
    topics = MagazineTopics.objects.all().order_by('topic_text')
    variables = {"topics":topics}
    return render(request, 'home.html', context=variables)
    # return render(request, 'home.html')


def about(request):
    return render(request, 'about.html')


def archive(request):
    return render(request, 'archive.html')

@login_required
def create_news(request):
    variables = {"id": -1, "news": None}
    return render(request, 'create_news_article.html', context=variables)

@login_required
def load_edit(request, id):
    news_id = id
    try:
        news_article = News.objects.get(id=news_id)
    except:
        return redirect_home()

    variables = {"id": news_article.id, "news": news_article}
    return render(request, 'create_news_article.html', context=variables)


def get_archive_articles(request):
    start_time = time.time()
    try:
        issue_id = int(request.GET.get('issue_id', None))
    except:
        issue_id = 'None'
    try:
        author_id = int(request.GET.get('author_id', None))
    except:
        author_id = 'None'
    try:
        topic_id = int(request.GET.get('topic_id', None))
    except:
        topic_id = 'None'
    article_search = request.GET.get('article_search', '')
    print "*********author: ", author_id
    print "*********aissue: ", issue_id
    page = int(request.GET.get('page', 1))

    if issue_id and issue_id not in ('None', None):
        issue_articles = MagazineArticles.objects.filter(issue_id=issue_id).order_by('-issue_id')
    elif author_id and author_id not in ('None', None):
        issue_articles = MagazineArticles.objects.filter(author_id=author_id).order_by('-issue_id')
    elif topic_id and topic_id not in ('None', None):
        issue_articles = MagazineArticles.objects.filter(topic_id=topic_id).order_by('-issue_id')
    else:
        issue_articles = MagazineArticles.objects.all()

    if article_search:
        print "****in search...: ", article_search
        issue_articles = issue_articles.filter(Q(header__icontains=article_search)|Q(text__icontains=article_search))
    print "*******count issuse arts: ", issue_articles.count()
    # pagination
    paginator = Paginator(issue_articles, 10)
    try:
        article_page = paginator.page(page)
    except PageNotAnInteger:
        article_page = paginator.page(1)
    except EmptyPage:
        article_page = paginator.page(paginator.num_pages)

    def get_final_article_list(articles):
        final_articles = []
        for item in articles:
            final_articles.append(item)
        return final_articles
    final_articles = get_final_article_list(article_page)
    last_page = paginator.num_pages
    page_range = get_page_range(article_page.number, last_page)
    print "*********Got %s articles in %ss: "%(len(final_articles), time.time()-start_time)
    variables = {"issue_articles": final_articles, "article_page":article_page, "issue_id":issue_id, "page_range":page_range, "last_page":last_page}
    return render(request, 'partial/archive_articles.html', context=variables)


def get_archive_years(request):
    magazine_issues = MagazineIssues.objects.all().order_by('-issue_id')
    years = list(set(magazine_issues.values_list('year', flat=True)))
    years.reverse()
    variables = {"magazine_issues":magazine_issues, "years":years}
    return render(request, 'partial/archive_years.html', context=variables)


def get_archive_authors(request):
    sort = int(request.GET.get('sort', 0))
    start_time = time.time()
    mag_articles = list(MagazineArticles.objects.all().values_list('author_id', flat=True))
    authors = MagazineAuthors.objects.all().order_by('author_id')
    authors = [model_to_dict(i) for i in authors]
    for author in authors:
        author['mag_count'] = mag_articles.count(author['author_id'])
    if sort == 0:
        final_authors = sorted(authors, key=lambda k: k['last_name'])
    else:
        final_authors = sorted(authors, key=lambda k: k['mag_count'])
        final_authors.reverse()
    print "*****took %ss to get archive authors with sort %s"%(time.time()-start_time, sort)
    variables = {"authors":final_authors}
    return render(request, 'partial/archive_authors.html', context=variables)


def view_article(request, id):
    article_id = id
    try:
        archive_article = MagazineArticles.objects.get(article_id=article_id)
        article_author = MagazineAuthors.objects.get(author_id=archive_article.author_id)
    except:
        return redirect_home()
    variables = {"article": archive_article, "author": article_author}
    return render(request, 'archive_article.html', context=variables)


def redirect_home():
    return HttpResponseRedirect('/')


def get_newsfeed(request):
    user = request.user
    page = request.GET.get('page', 1)
    orig_news = News.objects.all().order_by('-article_date')

    # pagination
    paginator = Paginator(orig_news, 10)
    try:
        news_page = paginator.page(page)
    except PageNotAnInteger:
        news_page = paginator.page(1)
    except EmptyPage:
        news_page = paginator.page(paginator.num_pages)

    def get_final_news_list(news):
        final_news = []
        for item in news:
            title = item.title
            article = re.sub('<.*?>', '', item.article)
            article = re.sub('&.*?;', '', article)
            article_date = item.article_date
            final_news.append([title, article, article_date, item])
        return final_news
    final_news = get_final_news_list(news_page)
    last_page = paginator.num_pages
    page_range = get_page_range(news_page.number, last_page)
    print "*************HERE: ", final_news
    variables = {"news":final_news, "news_page":news_page, "page_range":page_range, "last_page":last_page}
    return render(request, 'partial/home_newsfeed.html', context=variables)


def get_page_range(current_page_num, num_pages):
    dis_num = 4
    page_range = xrange(max(1, current_page_num-dis_num), min(current_page_num+dis_num+1, num_pages+1))
    return page_range


def view_news(request, id):
    news_id = id
    # path_info = request.META['PATH_INFO']
    # http_ref = request.META['HTTP_REFERER']
    # print "*******PATH INFO: ", path_info, http_ref, request.get_full_path()
    user = request.user
    print "***********USER: ", user, "*******setup id: ", id
    try:
        news_article = News.objects.get(id=news_id)
    except:
        return redirect_home()
    comment_count = CustomComment.objects.filter(content_type=ContentType.objects.get_for_model(News), object_pk=news_article.id)
    print "************", comment_count, news_article, news_article.id
    comment_count = comment_count.count()
    variables = {"news":news_article, "comment_count":comment_count}
    return render(request, 'news_article.html', context=variables)

@staff_member_required(login_url='/forbidden')
@login_required
def add_edit_news_article(request):
    news_id = int(request.POST["id"])
    title = request.POST["title"]
    preface = request.POST["preface"]
    article = request.POST["article"]
    picture = request.FILES.get("picture")
    errors = []
    if title == '':
        errors.append("Missing Title")
    if preface == "<p><br data-mce-bogus=\"1\"></p>":
        errors.append("Missing Preface")
    if article == "<p><br data-mce-bogus=\"1\"></p>":
        errors.append("Missing Article")
    if news_id == -1 and picture is None:
        errors.append("Missing Picture")

    if len(errors) == 0:
        if news_id == -1:
            new_news = News(title=title, preface=preface, article=article, preview_img_height=0, news_img_height=0, news_img_width=0, article_date=str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
    , active=1)
            new_news.save()
            pic_id = News.objects.all().order_by("-id")[0].id
            handle_uploaded_file(picture, pic_id)
            return HttpResponse(json.dumps({"success": "News article was upload successfully"}), content_type='application/json')
        else:
            to_edit = News.objects.get(id=news_id)
            to_edit.title = title
            to_edit.preface = preface
            to_edit.article = article
            to_edit.save()
            if picture is not None:
                if os.path.exists('./media/news_images/image-news-300-' + str(news_id) + '.jpg'):
                    os.remove('./media/news_images/image-news-300-' + str(news_id) + '.jpg')
                handle_uploaded_file(picture, news_id)
            return HttpResponse(json.dumps({"success": "Article has been updated"}), content_type='application/json')

    return HttpResponse(json.dumps({"errors": errors}), content_type='application/json')


def handle_uploaded_file(f, pic_id):
    with open(settings.MEDIA_ROOT + 'news_images/image-news-300-' + str(pic_id) + '.jpg', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

@staff_member_required(login_url='/forbidden')
@login_required
def disable_news_article(request):
    news_id = request.GET.get("id")
    try:
        to_delete = News.objects.get(id=news_id)
        to_delete.active = 0
        to_delete.save()
        return HttpResponse(json.dumps({"success": "Post Deleted Successfully"}), content_type='application/json')
    except:
        return HttpResponse(json.dumps({"error": "Could Not Delete Post"}), content_type='application/json')

@staff_member_required(login_url='/forbidden')
@login_required
def enable_news_article(request):
    news_id = request.GET.get("id")
    try:
        to_enable = News.objects.get(id=news_id)
        to_enable.active = 1
        to_enable.save()
        return HttpResponse(json.dumps({"success": "Post Enabled Successfully"}), content_type='application/json')
    except:
        return HttpResponse(json.dumps({"error": "Could Not Enable Post"}), content_type='application/json')


def check_send_confirm_email(request):
    username = request.POST['username']
    password = request.POST['password']
    errors = []
    if username == "":
        errors.append("Username или email не может быть пустым")
    if password == "":
        errors.append("Пароль не может быть пустым")
    if len(errors) == 0:
        try:
            user = User.objects.get(Q(email__iexact=username)|Q(username__iexact=username))
            if user.check_password(raw_password=password):
                user = user
            else:
                user = None
        except:
            user = None
        if user and not user.is_active:
            send_confirm_email(request, user)
        elif user and user.is_active:
            errors.append("Аккаунт уже активен!")
        else:
            errors.append("Что-то случилось при попытке отправить подтверждение!")
    variables = {"errors":errors}
    return HttpResponse(json.dumps(variables), content_type='application/json')

def send_confirm_email(request, user):
    current_site = get_current_site(request)
    message = render_to_string('partial/acc_active_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    text_content = strip_tags(message)
    mail_subject = 'Activate your blog account.'
    to_email = str(user.email)
    msg = EmailMultiAlternatives(mail_subject, text_content, to=[to_email])
    msg.attach_alternative(message, "text/html")
    msg.send()

def register_submit(request):
    print "******************GOT HERE"
    post_dict = request.POST
    first_name = post_dict.get('first_name')
    last_name = post_dict.get('last_name')
    username = post_dict.get('username')
    email = post_dict.get('email')
    password = post_dict.get('password')
    errors = []
    if first_name == '':
        errors.append("Пожалуйста, укажите имя")
    if last_name == '':
        errors.append("Укажите фамилию")
    if len(username) < 5:
        errors.append("Username должен содержать не менее 5 символов")
    elif len(username) > 30:
        errors.append("Username должно быть 50 символов или меньше")
    match = re.search(r'[^A-Za-z0-9_\.]+', username)
    if match:
        errors.append("Username может содержать только буквы, цифры, периоды и символы подчеркивания")
    if len(password) < 5:
        errors.append("Пароль должен содержать не менее 5 букв/символов")
    try:
        validate_email(email)
    except:
        errors.append('Введите действительный email')
    if User.objects.filter(username=username).exists():
        errors.append("Username уже занято")
    if User.objects.filter(email=email).exists():
        errors.append("Аккаунт с этим email уже существует")

    if len(errors) == 0:
        user = User(username=username, email=email, first_name=first_name, last_name=last_name, is_active=False)
        user.set_password(password)
        user.save()
        try:
            send_confirm_email(request, user)
            return HttpResponse(json.dumps({"success_msg": 'Пожалуйста, подтвердите свой email, чтобы завершить регистрацию'}), content_type='application/json')
        except:
            traceback.print_exc()
            return HttpResponse(json.dumps({"errors": ["Что-то случилось не так во время регистрации!"]}), content_type='application/json')


    return HttpResponse(json.dumps({"errors": errors}), content_type='application/json')


def activate(request, uidb64, token):
    print "******trying to activate: ", uidb64, token
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user and user.is_active:
        variables = {"activation_msg": 'Аккаунт уже активен', "activation_type":"info"}
    elif user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # login(request, user)
        variables = {"activation_msg": 'Спасибо за ваше подтверждение по электронной почте. Теперь вы можете войти в ваш аккаунт', "activation_type":"success"}
    else:
        variables = {"activation_msg": 'Недопустимая ссылка активации!', "activation_type":"danger"}
    return render(request, 'activation.html', context=variables)

def get_comments(request):
    news_id = request.GET.get('news_id')
    page =request.GET.get('page', 1)
    news_article = News.objects.get(id=news_id)
    comments = CustomComment.objects.filter(content_type=ContentType.objects.get_for_model(News), object_pk=news_article.id).order_by('-submit_date')
    print "**early commments: ", comments
    paginator = Paginator(comments, 10)
    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)
    print "*****comments:", comments
    final_comments = []
    for comment in comments:
        final_comments.append([comment, get_time_since_created(comment.submit_date)])
    print "Final comments: ", final_comments
    variables = {'comments':final_comments, 'news_article':news_article, 'comments_page':comments, 'page':page}
    return render(request, 'partial/comments_stub.html', context=variables)

#Helper functions
def get_time_since_created(datetime_value):
    seconds_since = (timezone.now() - datetime_value).total_seconds()
    if seconds_since < 60:
        time_string = "сек."
        time_value = (int(seconds_since))
    elif seconds_since < 3600:
        time_string = "мин."
        time_value = (int(seconds_since / 60))
    elif seconds_since < 86400:
        time_string = "час."
        time_value = (int(seconds_since) / 3600)
    elif seconds_since < 2592000:
        # time_string = "дней"
        time_value = (int(seconds_since) / 86400)
        time_dict = {1: "день", 2: "дня", 3: "дня", 4: "дня"}
        time_string = time_dict.get(time_value, "дней")
    elif seconds_since < 31536000:
        time_string = "мес."
        time_value = (int(seconds_since) / 2592000)
    else:
        # time_string = "лет"
        time_value = (int(seconds_since) / 31536000)
        time_dict = {1: "год", 2: "года", 3: "года", 4: "года"}
        time_string = time_dict.get(time_value, "лет")
    time_string += " назад"
    return str(time_value)+' '+time_string

def forbidden(request):
    return HttpResponseForbidden('<h1>403 Permission Denied</h1>')