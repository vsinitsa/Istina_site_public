import hashlib
import urllib
from django import template
from django.utils.safestring import mark_safe

register = template.Library()


# return only the URL of the gravatar
# TEMPLATE USE:  {{ email|gravatar_url:150 }}
@register.filter
def gravatar_url(email, size=40):
    default = "identicon"
    gravatar_url = "https://www.gravatar.com/avatar/%s?%s" % (hashlib.md5(email.lower()).hexdigest(), urllib.urlencode({'d': default, 's': str(size)}))
    print "******************grav url: ", email, gravatar_url
    return gravatar_url


@register.filter
def parse_article_imgs(article_str):
    sp = article_str.split('<img')
    temp_sp = sp[1:]
    for i, item in enumerate(temp_sp):
        if "style=" in item:
            temp_sp[i] = 'style="max-width:100% !important; height:auto;'.join(item.split('style="'))
        else:
            temp_sp[i] = ' style="max-width:100% !important; height:auto;"'+item
    final_str = '<img'.join([sp[0]]+temp_sp)
    return final_str
            
            