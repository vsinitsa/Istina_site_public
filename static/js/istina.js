
var set_error = function(error_msg){
    msg = '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+error_msg+"</div>";
    return msg;
};

var set_success = function(success_msg){
    msg = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+success_msg+"</div>";
    return msg;
};

var set_info = function(info_msg){
    msg = '<div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+info_msg+"</div>";
    return msg;
};

var set_warning = function(warning_msg){
    msg = '<div class="alert alert-warning alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+warning_msg+"</div>";
    return msg;
};

var start_login = function(){
    $('#username').val('');
    $('#password').val('');
    $('#login_modal').modal('show');
};

var send_confirm_email = function(){
    $('#login_spinner').css('display', '');
    $('#login_errors').html('');
    var success_login = function(response){
        $('#login_spinner').css('display', 'none');
        $('#login_errors').html('');
        if(response.errors.length > 0){
            for(var i=0; i<response.errors.length; i++){
                $('#login_errors').append(set_error(response.errors[i]));
            }
        }else{
            $('#base_errors').html('');
            $('#base_errors').append(set_info("Пожалуйста, подтвердите свой email, чтобы завершить регистрацию"));
            $('#login_modal').modal('hide');
        }
    };
    var username = $('#username').val();
    var password = $('#password').val();
    var data = {"username":username, "password":password, "csrfmiddlewaretoken":csrf_token};
    $.ajax({url:'/send_confirm_email', data:data, type:"POST", success:success_login})
};


var start_register = function(){
    $('#login_modal').modal('hide');
    setTimeout(function(){
        reset_register_modal();
        $('#reg_first_name').val('');
        $('#reg_last_name').val('');
        $('#reg_username').val('');
        $('#reg_email').val('');
        $('#reg_password').val('');
        $('#confirm_password').val('');
        $('#register_modal').modal('show');
    }, 500);
};

var reset_register_modal = function(){
    $('#register_submit').attr('disabled', false);
    $('#reg_spinner').css('display', 'none');
    $('#register_errors').html('');
    $('#username_errors').html('');
    $('#reg_email_errors').html('');
    $("#password_errors").html('');
    $('#confirm_pass_error').html('');
    $('#reg_username').css('border', '1px solid #cccccc');
    $('#reg_password').css('border', '1px solid #cccccc');
    $('#confirm_password').css('border', '1px solid #cccccc');
};

var register = function () {
    $('#register_submit').attr('disabled', true);
    $('#reg_spinner').css('display', '');
    var success_register = function (response) {
        reset_register_modal();
        if(response.success_msg){
            $('#base_errors').html(set_info(response.success_msg));
            $('#register_modal').modal('hide');
        }
        else if(response.errors.length > 0){
            for(var i=0; i<response.errors.length; i++){
                var err_str = response.errors[i];
                if (err_str.toLowerCase().indexOf("пароль") >= 0)
                    $('#password_errors').append(set_error(err_str));
                else if (err_str.toLowerCase().indexOf("username") >= 0)
                    $('#username_errors').append(set_error(err_str));
                else if (err_str.toLowerCase().indexOf("email") >= 0)
                    $('#reg_email_errors').append(set_error(err_str));
                else
                    $('#register_errors').append(set_error(err_str));
            }
        }else{
            window.location = '/';
        }
    };
    var first_name = $('#reg_first_name').val();
    var last_name = $('#reg_last_name').val();
    var username = $('#reg_username').val();
    var email = $('#reg_email').val();
    var password = $('#reg_password').val();
    var data = {"username":username, "first_name":first_name, "last_name":last_name, "email":email, "password":password, "csrfmiddlewaretoken":csrf_token};
    $.ajax({url:'/submit_register', data:data, type:"POST", success:success_register})
};

var check_confirm_password = function(ele){
    $('#confirm_pass_error').html("");
    $('#confirm_password').css('border', '1px solid #cccccc');
    var confirm_val = $(ele).val();
    var orig_pass = $('#reg_password').val();
    if (confirm_val != orig_pass){
        $('#confirm_pass_error').html(set_error("Подтверждение пароля не соответствует паролю"));
        $('#confirm_password').css('border', '1px solid red');
    }
};

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    };

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    if (pass.length < 5)
        return ["Пароль должен содержать не менее 5 букв/символов", 'red'];
    var score = scorePassword(pass);
    if (score > 70)
        return ["Надежный пароль", 'limegreen'];
    if (score > 45)
        return ["Хороший пароль", 'orange'];
    if (score <= 45)
        return ["Слабый пароль", 'red'];
    return ['',''];
}

var main_login = function () {
    var success_login = function(response){
        $('#login_errors').html('');
        if(response.errors){
            for(var i=0; i<response.errors.length; i++){
                $('#login_errors').append(set_error(response.errors[i]));
            }
        }else{
            window.location = response.next
        }
    };
    var username = $('#username').val();
    var password = $('#password').val();
    var next = window.location.href;
    var data = {"username":username, "password":password, "next":next, "csrfmiddlewaretoken":csrf_token};
    $.ajax({url:'/login_view', data:data, type:"POST", success:success_login})
};


var load_page = function(page){
    window.location = page;
};

var go_home = function(){
    sessionStorage.removeItem('gSearchText');
    sessionStorage.removeItem('home_page_number');
    load_page("/");
};

$(document).ready(function() {
    $("#article_search").keyup(function(event){
            if(event.keyCode == 13){
                $("#article_search_btn").click();
            }
    });
    $("#username").keyup(function(event){
            if(event.keyCode == 13){
                main_login();
            }
    });
    $("#password").keyup(function(event){
            if(event.keyCode == 13){
                main_login();
            }
    });

    $("#reg_password").on("keypress keyup keydown", function() {
        var pass = $(this).val();
        $('#reg_password').css('border', '1px solid #cccccc');
        var check_pass = checkPassStrength(pass);
        $("#password_errors").html(check_pass[0]);
        $("#password_errors").css('color', check_pass[1]);
        if (check_pass[0].length > 45){
            $('#password_errors').html(set_error(check_pass[0]));
            $('#reg_password').css('border', '1px solid red');
        }
    });

    $("#reg_username").on("keypress keyup keydown", function() {
        $('#username_errors').html('');
        var username = $(this).val();
        var text = '';
        $('#reg_username').css('border', '1px solid #cccccc');
        if (username.length < 5)
            text = "Username должен содержать не менее 5 символов";
        var reg = /[^A-Za-zА-Яа-я0-9_.]+/;
        if (reg.test(username))
            text = "Username может содержать только буквы, цифры, периоды и символы подчеркивания";
        if (text){
            $('#username_errors').html(set_error(text));
            $('#reg_username').css('border', '1px solid red')
        }
    });

    if (window.location.href.indexOf('/archive') !== -1){
        // var issue_id = $.session.get('issue_id');
        var author_id = sessionStorage.getItem('author_id');
        // var article_search = $.session.get('article_search');
        // var article_page = $.session.get('article_page');
        var tab_number = sessionStorage.getItem('tab_number');
        if (tab_number == 0){
            load_archive_years();
        }else if (tab_number == 1){
            load_archive_authors();
        }else if (tab_number == 2){
            if(author_id != null){
                load_author_articles(author_id);
            }else{
                load_all_articles_from_page_load();
            }
        }else{
            load_archive_years();
        }
    }

    $("#id_comment").focus(function () {
        $('#add_comment_btns').attr('hidden', false);
        $(this).attr('rows', 6);
    });

});

var clear_comment_section = function () {
    $('#add_comment_btns').attr('hidden', true);
    $('#id_comment').val('');
    $('#id_comment').attr('rows', 3);
    show_add_comment('');
};

var gCommentsPage = 1;

var get_comments = function(news_id, page) {
    var refresh_btn = $('#refresh_btn').html();
    if (refresh_btn != '<div class="loader"></div>'){
        $('#refresh_btn').html('<div class="loader"></div>');
    }
    if (page != -1){
        $('#load_more_comments'+(page-1)).remove();
    }else{
        gCommentsPage = 1;
    }
    var variables = {news_id:news_id, page:gCommentsPage};
    $.get("/get_comments", variables, function( data ) {
        if (page == -1){
            $('#comment_rows').html('');
        }
        $('#comment_rows').append(data);
        $('#refresh_btn').html('<span class="glyphicon glyphicon-refresh"></span>');
    })
};

var set_menu_item_active = function(page){
    var menu_items = $('#main_menu_items').children();
    for (var i=0; i < menu_items.length; i++){
        $(menu_items[i]).prop('class', '');
    }
    $('#'+page+'_menu_item').prop('class', 'active');
};


//Home stuff


var get_home_news = function (page) {
    $('body').removeClass('loaded');
    console.log("PAGE*************: ", page)
    if (page == -1){
        page = sessionStorage.getItem('home_page_number');
    }
    var gPage = page;
    var check_home = function(response){
        $('#home_news').html(response);
        $('body').addClass('loaded');
        if (gPage != -1){
            $(window).scrollTop(0);
        }
    };
    if (page == -1){
        page = 1;
    }
    sessionStorage.setItem('home_page_number', page);
    var data = {'page':page};
    $.ajax({url:'/get_newsfeed', data:data, success:check_home})
};


//Archive stuff

var load_archive_years = function () {
    // $('#main_archive').load('/get_archive_years');
    sessionStorage.setItem('tab_number', 0);
    set_archive_nav('years');
    $('#main_archive').html('');
    $('#loading_gif').css('display', 'block');
    $.get("/get_archive_years", {}, function( data ) {
        $('#main_archive').html(data);
        $('#loading_gif').css('display', 'none');
    });
    // $('#loading_gif').css('display', 'none');
};

var gArchiveAuthorSort = 0;
var load_archive_authors = function () {
    sessionStorage.setItem('tab_number', 1);
    set_archive_nav('authors');
    $('#main_archive').html('');
    $('#loading_gif').css('display', 'block');

    var variables = {'sort':gArchiveAuthorSort};
    $.get("/get_archive_authors", variables, function( data ) {
        $('#main_archive').html(data);
        $('#loading_gif').css('display', 'none');
        $('#sort_authors'+gArchiveAuthorSort).prop('class', 'active');
    });
    // $('#main_archive').load('/get_archive_authors');
};

var search_articles = function(){
    sessionStorage.setItem('author_id', '');
    sessionStorage.setItem('issue_id', '');
    sessionStorage.setItem('topic_id', '');
    var article_search = $('#article_search').val();
    sessionStorage.setItem('article_search', article_search);
    sessionStorage.setItem('article_page', gArticlePage);
    load_all_articles();
};

var gArticlePage = 1;
var load_all_articles = function() {
    sessionStorage.setItem('tab_number', 2);
    set_archive_nav('search');
    $('#main_archive').html('');
    $('#loading_gif').css('display', 'block');
    var article_search = $('#article_search').val();
    sessionStorage.setItem('article_search', article_search);
    sessionStorage.setItem('article_page', gArticlePage);
    var issue_id = sessionStorage.getItem('issue_id');
    var author_id = sessionStorage.getItem('author_id');
    var topic_id = sessionStorage.getItem('topic_id');
    var variables = {'page':gArticlePage, "article_search":article_search, 'author_id':author_id, 'issue_id':issue_id, "topic_id":topic_id};
    $.get("/get_archive_articles", variables, function( data ) {
        $('#main_archive').html(data);
        $('#loading_gif').css('display', 'none');
    });
};

var load_all_articles_from_page_load = function(){
    var page = sessionStorage.getItem('article_page');
    if (isNaN(page)){
        gArticlePage = 1;
    }else{
        gArticlePage = page;
    }
    var article_search = sessionStorage.getItem('article_search');
    $('#article_search').val(article_search);
    load_all_articles();
};

var load_issue_articles = function(issue_id){
    sessionStorage.setItem('issue_id', issue_id);
    sessionStorage.setItem('author_id', '');
    sessionStorage.setItem('tab_number', 2);
    set_archive_nav('search');
    $('#main_archive').html('');
    $('#loading_gif').css('display', 'block');

    var variables = {'issue_id':issue_id};
    $.get("/get_archive_articles", variables, function( data ) {
        $('#main_archive').html(data);
        $('#loading_gif').css('display', 'none');
    });
};

var load_author_articles = function(author_id){
    sessionStorage.setItem('author_id', author_id);
    sessionStorage.setItem('issue_id', '');
    sessionStorage.setItem('tab_number', 2);
    set_archive_nav('search');
    $('#main_archive').html('');
    $('#loading_gif').css('display', 'block');

    var variables = {'author_id':author_id};
    $.get("/get_archive_articles", variables, function( data ) {
        $('#main_archive').html(data);
        $('#loading_gif').css('display', 'none');
    });
};

var topic_to_archive = function(topic_id){
    sessionStorage.setItem('issue_id', '');
    sessionStorage.setItem('author_id', '');
    sessionStorage.setItem('article_search', '');
    sessionStorage.setItem('article_page', 1);
    sessionStorage.setItem('topic_id', topic_id);
    sessionStorage.setItem('tab_number', 2);
    load_page('/archive');
};

var set_archive_nav = function(func_name){
    var archive_nav = $('#archive_nav').children();
    for (var i=0; i<archive_nav.length; i++){
        $(archive_nav[i]).prop('class', '');
    }
    $('#archive_'+func_name).prop('class', 'active');
};

var add_edit_news_article = function(id){
    var post_success = function(response){
        $('#blog_errors').html('');
        $('#add_news_success').html('');
        if(response.errors){
            for(var i=0; i<response.errors.length; i++){
                $('#blog_errors').append(set_error(response.errors[i]));
            }
        }else{
            // $('#add_news_success').append(set_success("Upload Successful"));
            go_home();
        }
    };

    var title = $('#title').val();
    var preface = tinyMCE.get('preface').getContent({format:'raw'});
    var article = tinyMCE.get('article').getContent({format:'raw'});
    var picture = $('#picture')[0].files[0];
    var data = new FormData();
    data.append("id", id);
    data.append("csrfmiddlewaretoken", csrf_token);
    data.append("title", title);
    data.append("preface", preface);
    data.append("article", article);
    data.append("picture", picture);
    $.ajax({url:'/add_edit_news_article', data:data, type:"POST", contentType:false, cache:false,  processData:false, success: post_success});
};

var disable_news_article = function(id){
    var variables = {"id": id};
    $.get("/delete_news_article", variables, function(){
        go_home()
    });
};

var enable_news_article = function(id){
    var variables = {"id": id};
    $.get("/enable_news_article", variables, function(){
        go_home()
    });
};



//load tinymce
tinyMCE.init({
    /* replace textarea having class .tinymce with tinymce editor */
	selector: "textarea.tinymce",

	/* theme of the editor */
	theme: "modern",
	skin: "lightgray",

	/* width and height of the editor */
	width: "100%",

	/* display statusbar */
	statubar: true,

	/* plugin */
	plugins: [
		"advlist autolink link image lists charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"save table contextmenu directionality emoticons template paste textcolor"
	],

	/* toolbar */
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",

	/* style */
	style_formats: [
		{title: "Headers", items: [
			{title: "Header 1", format: "h1"},
			{title: "Header 2", format: "h2"},
			{title: "Header 3", format: "h3"},
			{title: "Header 4", format: "h4"},
			{title: "Header 5", format: "h5"},
			{title: "Header 6", format: "h6"}
		]},
		{title: "Inline", items: [
			{title: "Bold", icon: "bold", format: "bold"},
			{title: "Italic", icon: "italic", format: "italic"},
			{title: "Underline", icon: "underline", format: "underline"},
			{title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
			{title: "Superscript", icon: "superscript", format: "superscript"},
			{title: "Subscript", icon: "subscript", format: "subscript"},
			{title: "Code", icon: "code", format: "code"}
		]},
		{title: "Blocks", items: [
			{title: "Paragraph", format: "p"},
			{title: "Blockquote", format: "blockquote"},
			{title: "Div", format: "div"},
			{title: "Pre", format: "pre"}
		]},
		{title: "Alignment", items: [
			{title: "Left", icon: "alignleft", format: "alignleft"},
			{title: "Center", icon: "aligncenter", format: "aligncenter"},
			{title: "Right", icon: "alignright", format: "alignright"},
			{title: "Justify", icon: "alignjustify", format: "alignjustify"}
		]}
	]
});

