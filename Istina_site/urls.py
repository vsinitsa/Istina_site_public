"""istina URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from istina import views
from django.conf import settings
from django.conf.urls.static import static
#uncomment 404 500 error lines once on live server to serve custom error pages.
# handler404 = views.handler404
# handler500 = views.handler500
urlpatterns = [
    url(r'^admin', admin.site.urls),
    url(r'^comments/', include('django_comments.urls')),
    # url(r'^accounts/login/$', auth_views.login),
    # url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}),
    url(r'^login_view$', views.login_view),
    # url(r'^register/$', views.register),
    url(r'^submit_register$', views.register_submit),
    url(r'^send_confirm_email$', views.check_send_confirm_email),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate, name='activate'),
    # url(r'^password_reset/$', auth_views.password_reset, {'template_name': 'password_reset.html'}),
    url(r'^$', views.home),
    url(r'^about$', views.about),
    url(r'^archive$', views.archive),
    url(r'^blog$', views.create_news),
    url(r'^get_archive_years$', views.get_archive_years),
    url(r'^get_archive_authors$', views.get_archive_authors),
    url(r'^get_archive_articles$', views.get_archive_articles),
    url(r'^get_newsfeed$', views.get_newsfeed),
    url(r'^news/(?P<id>[0-9]+)$', views.view_news),
    url(r'^article/(?P<id>[0-9]+)$', views.view_article),
    url(r'^add_edit_news_article$', views.add_edit_news_article),
    url(r'^disable_news_article$', views.disable_news_article),
    url(r'^blog/(?P<id>[0-9]+)$', views.load_edit),
    url(r'^enable_news_article$', views.enable_news_article),
    url(r'^get_comments$', views.get_comments),
    url(r'^forbidden$', views.forbidden),
    url(r'^home.php$', views.home),
    # url(r'^fav_comment$', views.fav_comment),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
